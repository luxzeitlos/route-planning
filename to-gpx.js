import fs from 'fs/promises';
import papa from 'papaparse';
import Openrouteservice from 'openrouteservice-js';

(async () => {
  const dataStr = await fs.readFile('optimized.csv', { encoding: 'utf-8' });
  let { data } = papa.parse(dataStr.trim());
  data = data.slice(1);
  
  const rtepts = data.map(line => `
    <rtept lat="${line[20]}" lon="${line[21]}">
      <name>${line[0]}</name>
    </rtept>
  `.trim()).join('\n');

  const gpx = `
    <?xml version="1.0" encoding="UTF-8" standalone="no" ?>
    <gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" creator="Wikipedia"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
    <!-- Kommentare sehen so aus -->
    <metadata>
      <name>Dateiname</name>
      <desc>Validiertes GPX-Beispiel ohne Sonderzeichen</desc>
      <author>
      <name>Lux</name>
      </author>
    </metadata>
    <rte>
      <name>Route</name>
      <desc>Route</desc>
      
      <rtept lat="52.58421186968784" lon="13.310012339898549">
        <name>CCCV Lager Berlin</name>
      </rtept>

      ${rtepts}

      <rtept lat="52.58421186968784" lon="13.310012339898549">
        <name>CCCV Lager Berlin</name>
      </rtept>
      
    </rte>
    </gpx>
  `;

  console.log('wrote gpx');
  await fs.writeFile('gpx.gpx', gpx);
})();