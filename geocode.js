import 'dotenv/config'
import fs from 'fs/promises';
import papa from 'papaparse';
import Openrouteservice from 'openrouteservice-js';

const API_KEY = process.env.API_KEY;
const Geocode = new Openrouteservice.Geocode({ api_key: API_KEY });

(async () => {
  const dataStr = await fs.readFile('sofantenjagd.csv', { encoding: 'utf-8' });
  let { data } = papa.parse(dataStr.trim());
  const headers = data[0];
  data = data.slice(1);
  data = data.filter(x => x[0] != null);

  for(const line of data) {
    let address = line[0];
    if(!['Berlin', 'berlin'].includes(address)) {
      address = address + ', Berlin'
    }
    const { bbox } = await Geocode.geocode({
      text: address,
      size: 1,
      // boundary_circle: { lat_lng: [49.412388, 8.681247], radius: 50 },
      // boundary_bbox: [[49.260929, 8.40063], [49.504102, 8.941707]],
      boundary_country: ["DE"]
    });

    line[20] = bbox[1];
    line[21] = bbox[0];
    console.log(`geocoded ${line[0]}`);
  }

  headers[20] = 'lat';
  headers[21] = 'lng';

  fs.writeFile('geocoded.csv', [headers, ...data].map(l => l.map(cell => `"${cell}"`).join(',')).join('\n'));
})();