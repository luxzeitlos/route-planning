import 'dotenv/config'
import fs from 'fs/promises';
import papa from 'papaparse';
import Openrouteservice from 'openrouteservice-js';

const API_KEY = process.env.API_KEY;
const Optimization = new Openrouteservice.Optimization({api_key: API_KEY });

(async () => {
  const dataStr = await fs.readFile('geocoded.csv', { encoding: 'utf-8' });
  let { data } = papa.parse(dataStr.trim());
  const headers = data[0];
  data = data.slice(1);
  data = data.filter(x => x[0] != null);

  const jobs = data.map((line, idx) => ({
    // id: idx,
    // service: 900,
    // // amount: [1],
    // // skills: [1],
    location: [Number(line[21]), Number(line[20])],
    "id": idx,
    "service": 300,
    "amount": [
        1
    ],
    // "location": [
    //     1.98465,
    //     48.70329
    // ],
    "skills": [
        1
    ],
    "time_windows": [
        [
            0,
            60*60*100
        ]
    ]
  }));
  
  const optimization = await Optimization.optimize({
    jobs,
    vehicles: [
      // {
      //   id: 1,
      //   profile: 'driving-car',
        
      //   // capacity: [100],
      //   // skills: [1],
      // },
      {
        "id": 1,
        "profile": "driving-car",
        // start: [13.523841293144237, 52.436095956837256],
        // end: [13.310012339898549, 52.58421186968784],
        "start": [ 13.310012339898549, 52.58421186968784 ],
        "end": [ 13.310012339898549, 52.58421186968784 ],
        "capacity": [
            100
        ],
        "skills": [
            1,
        ],
        "time_window": [
            0,
            60*60*100,
        ]
      },
    ],
  });

  console.log('jobs', jobs);

  const steps = optimization.routes[0].steps.filter(x => x.type === "job")
  for(const [idx, step] of Object.entries(steps)) {
    data[step.id][22] = idx;
    data[step.id][23] = step.arrival;
  }

  data.sort((x, y) => x[22] - y[22]);

  headers[22] = 'idx';
  headers[23] = 'arrival';

  console.log('optimized');

  fs.writeFile('optimized.csv', [headers, ...data].map(l => l.map(cell => `"${cell}"`).join(',')).join('\n'));
})();